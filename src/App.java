import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class App {
    public static void array(){
        Random random = new Random();
        Long[] arr = new Long[100];
        //create list number
        for (int i = 0; i<arr.length; i++) {
            arr[i] = Long.valueOf(random.nextLong());
        }

        ArrayList<Long> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arr);
        System.out.println(arrayList);
    }

    public static void array2(){
        Random random = new Random();
        Double[] arr2 = new Double [100];
        //create list number 
        for (int i=0; i<arr2.length; i++) {
            arr2[i] = Double.valueOf(random.nextDouble());
        }

        ArrayList<Double> arrayList2 = new ArrayList<>();
        Collections.addAll(arrayList2, arr2);
        System.out.println(arrayList2);
    }
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, j51.60!");
        //array();
        array2();
    }
}
