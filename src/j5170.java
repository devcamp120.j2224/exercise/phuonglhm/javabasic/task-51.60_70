import java.util.ArrayList;

public class j5170 {

    public static void removeElementInArrayList(){ //Xoá object o khỏi ArrayList, object o này phải chứa trong ArrayList
        // create an empty ArrayList
        ArrayList<String> names = new ArrayList<String>(7);
        names.add("Bharat");
        names.add("Chetan");
        names.add("Dinesh");
        names.add("Bharat");
        names.add("Falguna");
        names.add("Dinesh");
        names.add("Bharat");
        System.out.println("ArrayList before using remove method: ");
        for(int a = 0; a < names.size(); a++)
        {
        System.out.println(names.get(a).toString());
        }
        // removing first occurrence of "Bharat" and "Dinesh"
        names.remove("Bharat");//3.Xoá object o khỏi ArrayList, object o này phải chứa trong ArrayList.
        names.remove("Dinesh");
        names.remove(0); //4.Xoá một phần tử tại vị trí index
        names.set(1,"New Element set at index 1"); //  5.Cập nhật phần tử tại vị trí index
        System.out.println("ArrayList after using remove method: ");
        for(int a = 0; a < names.size(); a++) //8.int size(): lấy số lượng phần tử chứa trong ArrayList
        {
        System.out.println(names.get(a).toString()); //7. Object get(int index): Return object tại vị trí index trong ArrayList.
        }
    }

    public static void lastIndexOfMethod(){
        String s1 = "this is index of example";
        int index1 = s1.lastIndexOf('s'); //1.lấy vị trí xuất hiện cuối cùng của phần tử
        int index2 = s1.lastIndexOf("ex");
        System.out.println(index1);//6 
        System.out.println(index2);//17
    }

    public static void addElementToArrList(){
        //Thêm một phần tử vào vị trí được chỉ định. 
        //ArrayList.add(int index, E element) method to add element to specific index of ArrayList
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(1);
        arr.add(3);
        arr.add(1, 8);//2.Thêm một phần tử vào vị trí index. 
        arr.add(3);
        for (Integer i : arr) {
            System.out.print(i + " "); // 1 8 3 3
        }
        System.out.println("Position of 1st '3': " + arr.indexOf(3)); //6. int indexOf(Object o): Lấy vị trí index của object o phù hợp đầu tiên trong ArrayList.
        System.out.println("This ArrayList contains 2: " + arr.contains(2)); //9.Kiểm tra phần tử object o có chứa trong ArrayList, nếu có return true, ngược lại false.
    }

    public static void deleteMethod(){
        ArrayList<Integer> arr = new ArrayList<Integer>();
        arr.add(1);
        arr.add(2);
        arr.add(3);
        System.out.println("Size truoc khi xoa: " + arr.size());
        arr.clear();
        System.out.println("Size sau khi clear: " + arr.size());
    }

    public static void main(String args[]) {
        //lastIndexOfMethod();
        //addElementToArrList();
        //removeElementInArrayList();
        deleteMethod();

    }
}

      

